package br.com.mastertech.itau.imersivo.formas;

public class Calculador {
	public static double calculaAreaQuadrado(Quadrado quadrado) {
		return quadrado.lados.get(0) * quadrado.lados.get(0);
	}

	public static double calculaAreaCirculo(Circulo circulo) {
		return (Math.PI * Math.pow(circulo.lados.get(0), 2));
	}
	
	public static double calculaAreaTriangulo(Triangulo triangulo) {
		return (triangulo.lados.get(0) * triangulo.lados.get(1) * triangulo.lados.get(2)) / 2;
	}
}
