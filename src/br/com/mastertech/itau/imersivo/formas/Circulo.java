package br.com.mastertech.itau.imersivo.formas;

import java.util.ArrayList;

public class Circulo extends Forma{
	
	public Circulo(ArrayList<Double> lados) {
		this.lados = new ArrayList<>();
		this.lados = lados;
	}
	
	@Override
	double calcularArea() {
		return Calculador.calculaAreaCirculo(this);
	}

	@Override
	void toString(double area) {
		Impressora.imprimirMensagem("A area do Circulo é: " + area);
	}

}
