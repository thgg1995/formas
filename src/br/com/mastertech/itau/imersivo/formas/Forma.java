package br.com.mastertech.itau.imersivo.formas;

import java.util.ArrayList;

public abstract class Forma {
	protected ArrayList<Double> lados;
	
	abstract double calcularArea();
	
	abstract void toString(double area);
	
}
