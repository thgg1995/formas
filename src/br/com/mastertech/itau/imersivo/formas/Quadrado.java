package br.com.mastertech.itau.imersivo.formas;

import java.util.ArrayList;

public class Quadrado extends Forma {

	public Quadrado(ArrayList<Double> lados) {
		this.lados = new ArrayList<>();
		this.lados = lados;
	}
	
	@Override
	double calcularArea() {
		return Calculador.calculaAreaQuadrado(this);
	}

	@Override
	void toString(double area) {
		Impressora.imprimirMensagem("A area do Quadrado é: " + area);	
	}

}
