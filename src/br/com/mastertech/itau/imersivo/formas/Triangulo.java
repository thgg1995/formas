package br.com.mastertech.itau.imersivo.formas;

import java.util.ArrayList;

public class Triangulo extends Forma {

	public Triangulo(ArrayList<Double> lados) {
		this.lados = new ArrayList<>();
		this.lados = lados;
	}
	
	@Override
	double calcularArea() {
		return Calculador.calculaAreaTriangulo(this);
	}

	@Override
	void toString(double area) {
		Impressora.imprimirMensagem("A area do Triangulo é: " + area);
	}

}
