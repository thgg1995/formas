package br.com.mastertech.itau.imersivo.formas;

public class Impressora {
	
	public static void imprimirMensagem(String texto) {
		System.out.println(texto);
	}
	
	public static void imprimirInicio() {
		Impressora.imprimirMensagem("Olá! bem vindo ao calculador de área 3 mil!");
		Impressora.imprimirMensagem("Basta informar a medida de cada lado que eu te digo a área :)");
		Impressora.imprimirMensagem("Vamos começar!");
		Impressora.imprimirMensagem("");
		Impressora.imprimirMensagem("Obs: digite -1 se quiser parar de cadastrar lados!");
		Impressora.imprimirMensagem("");
	}

}
