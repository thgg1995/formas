package br.com.mastertech.itau.imersivo.formas;

import java.util.ArrayList;
import java.util.Scanner;

public class Validador {

	public static Forma validaForma(ArrayList<Double> lados) {
		if (lados.size() == 1) {
			Impressora.imprimirMensagem("Eu identifiquei um circulo!");
			return new Circulo(lados);
		} else if (lados.size() == 2) {
			Impressora.imprimirMensagem("Eu identifiquei um quadrado/retangulo!");
			return new Quadrado(lados);
		} else if (lados.size() == 3) {
			double ladoA = lados.get(0);
			double ladoB = lados.get(1);
			double ladoC = lados.get(2);
			if ((ladoA + ladoB) > ladoC && (ladoA + ladoC) > ladoB && (ladoB + ladoC) > ladoA) {
				Impressora.imprimirMensagem("Eu identifiquei um triangulo!");
				return new Triangulo(lados);
			} else {
				Impressora.imprimirMensagem("O triangulo informado era inválido :/");
				return null;
			}
		} else {
			Impressora.imprimirMensagem("Forma inválida!");
			return null;
		}
	}

	public static void validaMaiorQueZaro(double numero, Scanner scanner) {

		Impressora.imprimirMensagem("O lado deve ter um valor maior que zero, digite novamente");
		String texto = scanner.nextLine();
		validaNumero(texto, scanner);
	}

	public static boolean desejaMaisLados(Scanner scanner) {
		String texto = "";
		Impressora.imprimirMensagem("Deseja adicionar um novo lado(y/n)?");
		texto = scanner.nextLine();

		if (texto.equals("y")) {
			return true;
		} else if (texto.equals("n")) {
			return false;
		} else {
			Impressora.imprimirMensagem("Ação não especificada");
			return desejaMaisLados(scanner);
		}
	}

	public static double validaNumero(String texto, Scanner scanner) {
		try {
			double numero = 0;

			numero = Double.parseDouble(texto);

			if (numero <= 0) {
				validaMaiorQueZaro(numero, scanner);
			}

			return numero;

		} catch (Exception e) {
			Impressora.imprimirMensagem("Formato do numero esta incorreto, poderia digitar novamente, por gentileza?");
			texto = scanner.nextLine();
			return validaNumero(texto, scanner);
		}
	}
}
