package br.com.mastertech.itau.imersivo.formas;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class App {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		boolean deveAdicionarNovoLado = true;
		ArrayList<Double> listaLados = new ArrayList<>();

		Impressora.imprimirInicio();

		while (deveAdicionarNovoLado) {
			System.out.println("Informe o tamanho do lado " + (listaLados.size()));

			double tamanhoLado = Validador.validaNumero(scanner.nextLine(), scanner);

			listaLados.add(tamanhoLado);

			deveAdicionarNovoLado = Validador.desejaMaisLados(scanner);
		}

		Impressora.imprimirMensagem("Lados cadastrados!");
		Impressora.imprimirMensagem("Agora vamos calcular a área...");
		
		Forma forma = Validador.validaForma(listaLados);

		if (forma != null) {
			forma.toString(forma.calcularArea());

		}

	}
}
